#The master source file for the metabandit
#Jakob Coles, Shengli Zhu, Sihong Xie

import sys
sys.path.insert(0,'./bandits/TS')
sys.path.insert(0,'./bandits/GPUCB')
sys.path.insert(0,'./bandits/GraphUCB')
from TS import *
from Main_v12 import *
from UCB import *
sys.path.insert(0,'./data/')
from data_extract import *
from random import seed
import json
import pickle as pkl
from scipy.sparse import vstack, hstack
from scipy.sparse.linalg import norm
from random import sample 
import argparse
import pandas as pd


parser = argparse.ArgumentParser()

parser.add_argument("--arms_list",help="File listing base bandits in the format 'Bandit type-aggregation func-parameters', e.g. TS-min-1.000000'")
parser.add_argument("--meta_method",default="",help="Method used by the metabandit to pick a base bandit, e.g. UCB, egreedy, etc.") 
parser.add_argument("--data",default=".",help="Directory where the training data is stored")
parser.add_argument("--C",type=float,default=0.0,help="Hyperparameter used if the metabandit uses UCB")
parser.add_argument("--niters",type=int,default=300,help="Number of iterations to train for")
parser.add_argument("--epsilon",type=float,default=0.0,help="Hyperparameter used if the metabandit uses epsilon greedy")
parser.add_argument("--outdir",default=".",help="Directory to dump raw csv files")
parser.add_argument("--cluster_dir",default=".",help="Directory where cluster results are stored for GraphUCB metabandits")

args = parser.parse_args()


data_dir = args.data


#Load mentions and labels for training
initParm = {}
try:
    initParm = dict(
        pair_mid=data_dir+'raw/pair_mention/',
        id_file=data_dir+'raw/id/',
        path_orisentence=data_dir+"raw/mapping/mid_origin/",
        pair_label=data_dir+'label/pairs_label/pair_label.json',
        cs_label=data_dir+'label/mention_label/mention_label.json',
        ground_truth=data_dir+'label/mention_label/ground_truth.json',
        path_pickle=data_dir+"feature/new_pickle_sparse/",
        feature_json_path=data_dir+'feature/new_json_index/'
    )
except:
    print("Error: Training data not found")
    exit(1)
ex = data_extract(initParm)

#graph_embeddings = {}

#Annotates the mentions in one or more bags
def annotate_bags(bag_ids, balancing = False, nvp = 1):
    """
    Given a list of bags, construct labeled training data from these bags.
    :param bag_ids: a list of bag_id, each of which is product_name;pair_name
    :param balancing: whether to downsample the negatives
    :param nvp: negative vs. positive ratio
    
    :return X: matrix of mention feature vectors. May be affected by downsampling.
    :return y: array of mention labels. May be affected by downsampling.
    :return mids: array mention ids of the same order as X and y. Not affected by downsampling.
    :return total_pos: number total positives in these bags. Not affected by downsampling.
    :return total_annotated: number total annotated in these bags. Not affected by downsampling.
    """    
    X = []
    y = []
    mids = []
    pos_ix, neg_ix = [], []
    total_annotated = 0


    for b in bag_ids:
        product_name, pair = b.split(';')
        total_annotated += len(bag_mentions[product_name][pair])
        
        for m_id in bag_mentions[product_name][pair]:
            X.append(X_dict[m_id])
            y.append(y_dict[m_id])
            mids.append(m_id)
            # record the positive mentions
            if y[-1] == 1:
                pos_ix.append(len(y) - 1)
            else:
                neg_ix.append(len(y) - 1)
    
    total_pos = len(pos_ix)
    if not balancing or total_pos == 0:
        X = vstack(X)
        return X, y, mids, total_pos, total_annotated
    
    balanced_X, balanced_y = [], []
  
    if len(neg_ix) >= nvp * total_pos:
        s_neg_ix = sample(neg_ix, nvp * total_pos)
    else:
        s_neg_ix = neg_ix
    
    all_ix = s_neg_ix + pos_ix
    for ix in all_ix:
        balanced_X.append(X.tocsr()[ix])
        balanced_y.append(y[ix])
        
    return vstack(balanced_X), balanced_y, mids, total_pos, total_annotated

#various cleaning code from Sihong's original notebook that needs to be executed before every metabandit trial to refresh the mention lists, etc.
# load from path_pickle, key = mention_id, value = sparse mention feature vectors
X_dict = ex.read_pickle_sparse()
    
# load from ground_truth, key = mention_id, value = ground truth of the mentions
y_dict = ex.get_all_truth()
    
# load from pair_mid, key = product, value = dict of {bag_id: [mention_ids]}
# include   and positive bags (according to DS labeling). 4854 in total.
bag_mentions = ex.read_pair_mention()

# load from pair_label to determine label of bags
# key = product, value = [positive pairs according to DS]
pos_bags = ex.read_positive()
print('number of DS positive bags:',sum([len(pos_bags[p]) for p, _ in pos_bags.items()]))

for p, bags in pos_bags.items():
    pos_bags[p] = set(bags)

# whether to exclude negative bags based on distant supervision labels.
filter_ds_negative_bags = True

bag_labels = {}
bag_size = {}

assert len(X_dict) == len(y_dict), 'number of feature vectors != number of labels'

# inverse index
mention_to_bag = {}
empty_bags = []
empty_vectors = []
num_mentions = 0
num_ds_pos_mentions = 0
num_pos_mentions = 0
num_pos_bags = 0

# iterate the bag-mention structure.
d = 0
for product, bag_dict in bag_mentions.items():
    for bag, mention_list in bag_dict.items():
        bag_id = product + ';' + bag
            
        if filter_ds_negative_bags:
            if bag not in pos_bags[product]:
                continue
                
        # negative initially
        bag_labels[bag_id] = 0
                
        bag_size[bag_id] = len(mention_list)
        
        if len(mention_list) == 0:
            empty_bags.append(bag_id)
            continue

        for m_id in mention_list:
            mention_to_bag[m_id] = bag_id
            assert m_id in X_dict, 'mention id not found in X_dict'
            assert m_id in y_dict, 'mention id not found in y_dict'
            d = X_dict[m_id].shape[1]
            
            # if any mention in there is positive, it is a positive bag
            num_mentions += 1
            if y_dict[m_id] == 1:
                bag_labels[bag_id] = 1
                num_pos_mentions += 1
                
        if bag_labels[bag_id] == 1:
            num_pos_bags += 1
        
        if bag in pos_bags[product]:
            num_ds_pos_mentions += bag_size[bag_id]
    
#The metabandit code begins here

seed(42) #for consistency with random trials
k = 3 #Algorithm begins by labeling the k largest bags
base_bandits = {}
#How to pick the best bag: highest min score, highest mean score, or highest max score
aggregation_funcs = {"min":lambda a:np.min(a),"avg":lambda a:np.mean(a),"max":lambda a:np.max(a)}
with open(args.arms_list,"r") as arms_list_file:
    for line in arms_list_file.readlines():
        base_bandit_name = line[:-1].split("-") 
        base_bandit_type = base_bandit_name[0]#TS, GP, GU currently supported
        try:
            aggregation_func = aggregation_funcs[base_bandit_name[1]] #min,avg,max currently supported 
        except:
            print("Error: aggregation func %s not found"%base_bandit_name[1])
            exit(1)
        #q value for TS, beta for GP, iter number and walk length for GraphUCB
        base_bandit_hyperparameters = base_bandit_name[2]
        if base_bandit_type == "TS":
            base_bandits["-".join(base_bandit_name)] = TS(d,aggregation_func,float(base_bandit_hyperparameters))
        elif base_bandit_type == "GP":
            base_bandits["-".join(base_bandit_name)] = GPUCB(ex,k,aggregation_func,float(base_bandit_hyperparameters))
        elif base_bandit_type == "GU":
            cluster_path = "%s/cluster_iter%s_walk%s.json"%(args.cluster_dir,base_bandit_hyperparameters.split("_")[0],base_bandit_hyperparameters.split("_")[1])
            base_bandits["-".join(base_bandit_name)] = UCB(cluster_path,num_pos_mentions,bag_labels,bag_mentions,empty_bags,aggregation_func,initParm["ground_truth"])
        else:
            print("Base bandit type %s not found"%base_bandit_type)
            exit(1)


#The main program loop
    
#Find and print invalid bags (if any)
for bag_id, bag_label in bag_labels.items():
     product, pair = bag_id.split(';')
     try:
        mention_list = bag_mentions[product][pair]
     except:
        print(bag_id)

balancing = False

# training parameters for TS
lr = 0.05
n_iter = 10

num_positives_annotated = []
num_mentions_annotated = []
bag_id_annotated = []
    

sorted_bags = sorted([(b, bag_size[b], b_label) for b, b_label in bag_labels.items()], key = lambda x:x[1], reverse=True)

annotated_bags = set()


# add the ids of the bags from the top k largest bags
for i in range(k):
    annotated_bags.add(sorted_bags[i][0])
# extract product id and bag pair name
selected_bag_ids = [a[0] for a in sorted_bags[:k]]

X, y, mids, total_pos, total_annotated = annotate_bags(selected_bag_ids, balancing)
    
#Train every arm on the initial bag selection (k largest bags)
for b in base_bandits.values():
    if "TS" in str(type(b)):
        b.Fit(X.tocsr(), y, n_iter, lr)
    elif "GP" in str(type(b)):
        for u in range(3):
            b.learn(u)
    elif "UCB" in str(type(b)):
        for u in range(3):
            b.UCB_iteration(u,True)
                
Q = {k:0.0 for k in base_bandits} #reward estimator
N = {k:0.0 for k in base_bandits} #number of mentions annotated by each arm
N2 = {k:0.0 for k in base_bandits} #number of pos mentions annotated by each arm
N3 = {k:0.0 for k in base_bandits} #number of bags annotated by each arm

meta_metric = "recall" #always recall these days, precision is deprecated
outfile = open(args.outdir+"/metabandit_%s.csv"%"-".join(list(base_bandits.keys())),"w") #raw csv dump
#Recall(true) and precision(true) are ground truth recall and precision (no estimator used)
#Q[arm]N[arm],N2[arm] are the values of the above dictionaries for the arm chosen in the corresponding iteration
#Variance[arm] is the exploration term of UCB, if UCB is used (otherwise 0.0)
print("Iteration,Arm,Recall(true),Precision(true),Q[arm],N[arm],N2[arm],Variance[arm],Bagid",file=outfile)
hardcoded_max_a = sample(list(Q.keys()),k=1)[0] #for the meta_method of picking a single random arm
#main program loop
for i in range(args.niters):
    bag_scores = []
    #different methods for picking best base bandit
    ####UCB####
    meta_method = args.meta_method        
    if meta_method == "UCB":
        max_a = ""
        max_gain = -np.inf
        for a in Q:
            temp = np.inf
            if N[a]:
                temp = Q[a]+args.C*np.sqrt(np.log(i)/N[a])
            if temp > max_gain:
                max_gain = temp
                max_a = a
    ####RANDOM ARM (SINGLE ARM)####
    if meta_method == "random_one":
        max_a = hardcoded_max_a 
    ####RANDOM ARM (ALL ARMS)####
    if meta_method == "random_all":
        max_a = sample(list(Q.keys()),k=1)[0]
    #greedy
    if meta_method == "greedy":
        max_a = ""
        max_gain = -np.inf
        for a in Q:
            if Q[a] > max_gain:
                max_gain = Q[a]
                max_a = a
    #epsilon greedy
    if meta_method == "egreedy": 
        max_a = ""
        max_gain = -np.inf
        for a in Q:
            if Q[a] > max_gain:
                 max_gain = Q[a]
                 max_a = a
        if np.random.random_sample() < args.epsilon:
            max_a = sample(list(Q.keys()),k=1)[0]

    print(max_a) #best base bandit

    total_annotated, total_pos = 0,0
    selected_bag_id = ""
    mids = []
    #We now get the highest scoring bag and train the model (for GP this is mostly handled in the GP class)
    if "GP" in str(type(base_bandits[max_a])):
        #mod GP methods to weight by cluster
        prod, pair = base_bandits[max_a].learn(i+k)
        selected_bag_id = prod+";"+pair
        X, y, mids, total_pos, total_annotated = annotate_bags([selected_bag_id], balancing)
    elif "TS" in str(type(base_bandits[max_a])):
        for bag_id, bag_label in bag_labels.items():
            # skip the annotated or empty bags
            if bag_id in annotated_bags or bag_id in empty_bags:
                continue

            product, pair = bag_id.split(';')
            try:
                mention_list = bag_mentions[product][pair]
            except:
                print(bag_id)

            all_scores = []
            for m_id in mention_list:
                all_scores.append(base_bandits[max_a].TSpredict1(X_dict[m_id]))
                
                        
            bag_score = base_bandits[max_a].aggregation_func(all_scores)
            bag_scores.append((bag_id, bag_score))
        # sort bag scores
        bag_scores = sorted(bag_scores, reverse = True, key = lambda x:x[1])

        # label the bag with the highest score and re-fit the TS
        selected_bag_id = bag_scores[0][0]
        annotated_bags.add(selected_bag_id) 
        
        # X is a csr_matrix (rows=feature vectors), y is an python list of ground truth labels
        X, y, mids, total_pos, total_annotated = annotate_bags([selected_bag_id], balancing)
        #for bandit in base_bandits:
        #    base_bandits[bandit].Fit(X.tocsr(), y, n_iter, lr)
        base_bandits[max_a].Fit(X.tocsr(), y, n_iter, lr) #FIX FIX FIX
    elif "UCB" in str(type(base_bandits[max_a])):
        selected_bag_id = base_bandits[max_a].UCB_iteration(i,False)
        X, y, mids, total_pos, total_annotated = annotate_bags([selected_bag_id], balancing)            
        
    #some GP housekeeping
    for b in base_bandits:
        if "GP" in str(type(b)):
            b.bags_used.add(selected_bag_id)

    num_mentions_annotated.append(total_annotated)
    num_positives_annotated.append(total_pos)
        
    #precision deprecated
    if meta_metric=="precision":
        Q[max_a] = (Q[max_a]*N[max_a]+total_pos)/(N[max_a]+total_annotated)
    if meta_metric=="recall":
        #the main N and Q updates for UCB.
        #we can't get recall directly so we estimate the recall of each arm as the proportion of positives that
        #were labelled by the arm, divided by the proportion of times we have used that arm (the N3 trick)
        N2[max_a] += total_pos
        Q[max_a] = (N2[max_a]/sum(num_positives_annotated))/(N3[max_a]/(i+1)) if sum(num_positives_annotated) and N3[max_a] else 0.0
    N[max_a] += total_annotated
    N3[max_a] += 1.0

    #write to data dump
    print("%d,%s,%f,%f,%f,%f,%f,%f,%s"%(i,max_a,float(sum(num_positives_annotated))/num_pos_mentions,
                                            float(sum(num_positives_annotated))/float(sum(num_mentions_annotated)),
                                            Q[max_a],N[max_a],N2[max_a],args.C*np.sqrt(np.log(i)/N[max_a]),selected_bag_id),file=outfile)
outfile.close()
