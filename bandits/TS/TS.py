import numpy as np;
import scipy.sparse as sp

"""
# Thompson Sampling class (Regularized logistic regreession with batch updatas)
# see the paper:
    An Empirical Evaluation of Thompson Sampling, NIPS, 2011.
"""
class TS:
    def __init__(self, d, aggregation_func, q=0.0):  # d is the size of the feature vector
        # weight vector
        self.w = sp.csr_matrix(np.array([0.0] * d))
        # weight vector
        self.m = sp.csr_matrix(np.array([0.0] * d))
        # precision vector of the same shape as w
        self.q = sp.csr_matrix(np.array([q] * d))
        self.aggregation_func = aggregation_func
    # sigmoid function
    def sigmoid(self, w, x):

        if type(x).__name__ == 'csr_matrix':
            # expm1(x) calculate exp(x)-1
            return 1 / ((np.dot(-x, w.T)).expm1().A + 1 + 1)

        return 1 / (1 + np.exp(-np.dot(x, np.transpose(w))))

    # A function to fing the average value of a list
    def findAvg(self, predict):
        avg = sum(predict) / len(predict)
        return avg

    # A function to update the predicted function
    def Fit(self, X, y, K, a):
        """
        Update w, m , and q according to Algorithm 3 of the paper
            X: feature vectors
            y: labels
            K: number of iterations in gradient descent
            a: step size in gradient descent
        """
        # n: number of feature vectors
        # d: size of feature vector
        n, d = X.shape
        last_grad = 0.0
        # optimize w for the current batch
        for k in range(K):
            # this is the gradient of the q * (w-m) term
            # m is not changing in the optimization of the loss
            grad = self.q.multiply(self.w - self.m)

            for j in range(n):
                # Note: .expm1() is exponential minus 1 for sparse matrix
                #       and convert to a dense one and then +1
                # this is the gradient of the logistic regression loss function
                # grad += 1 / (1 + (-y[j] * (np.dot(X[j], self.w.T))).expm1().A + 1) *\
                #         ((-y[j] * (np.dot(X[j], self.w.T))).expm1().A + 1) * (-y[j] * X[j])

                y_hat = self.sigmoid(self.w, X[j])
                grad += (y_hat - y[j]) * X[j]

            self.w -= sp.csr_matrix(a * grad)

            last_grad = grad

        # update m for the next round
        self.m = self.w

        # update the q (uncertainty in each dimension of w)
        # D: diagonal matrix with diagonal entries being p(1-p)
        # p: p is the sigmoid of the features
        D = np.zeros(shape=(n, n))
        for i in range(n):
            v = self.sigmoid(self.w, X[i]) * (1 - self.sigmoid(self.w, X[i]))
            D[i][i] = v
        D = sp.csr_matrix(D)

        # dq = the diagonal entries of (X.T*D*X)
        # Note dimension here:
        # D: n-by-n diagonal matrix
        # X: n-by-d matrix

        dq = (np.dot(np.dot(np.transpose(X), D), X)).diagonal()
        self.q += sp.csr_matrix(dq)

    # return list of mention scores for all mentions in list X
    def TSpredict1(self, X):
        """
        Given feature vectors, predict probability P(Y=1|X)
        :param X:
        :return:
        """
        p_total = []
        for i, x in enumerate(X):
            prior = self.sigmoid(self.w, x)
            score = prior[0][0]
            p_total.append(score)

        return p_total  # prior of each mention
