"""
Created on Sat Jun 22 18:04:36 2019

@author: scheb
"""

"""Code is based upon: https://github.com/tushuhei/gpucb/blob/master/gpucb.py """

#IMPORTANT: WHERE TO READ LABEL DATA: 
#Use read_positive to get positive bag ids, then use mapping pair2mentionID to get mentions, and then use read_ground_truth to get label of the mentions 
#  Use the return dictionary of read_ground_truth to get the label
#  Linear kernel (but most important is to use rank-one-update)
import sys, os
sys.path.append('../../data/')

#from mymodule import MyModule
import numpy as np
#from mpl_toolkits.mplot3d import Axes3D
#from matplotlib import pylab as plt
import matplotlib.pyplot as plt
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C

from data_extract import *
import json
import random
import collections



class GPUCB(object):

    def __init__(self, ex, k, beta=100.):
        '''
        ex: an object from data_extract.py that has access to methods to read the dataset
        
        beta (optional): Hyper-parameter to tune the exploration-exploitation
        balance. If beta is large, it emphasizes the variance of the unexplored
        solution solution (i.e. larger curiosity).
        
        '''
        
        
        '''
        Dictionaries 
        '''
        #self.kernel_function = C(1.0, constant_value_bounds="fixed") * RBF(1.0, length_scale_bounds="fixed") #the default kernel from GP, may change later 
        self.bag2Mentions = ex.read_pair_mention() #a dictionary of the form {bag_id: mention_list}
        self.mention2Index = {} #a dictionary linking the mention ID to the index 
        self.bagsSizeOne = set()#the size of each bag
        self.bag2Scores = collections.defaultdict(dict) #a dictionary of the form {bag_id: average_score_of_mentions} 
        self.labels_dictionary = ex.get_ppm_truth() #a dictionary of the form {mention_id: label}
        self.positiveProductMap = ex.read_positive() #format: {prod_name: [pos_pair1, pos_pair2, ... ]}    
        self.labels_matrix = np.array([])    #the actual labels matrix 
        self.features_dictionary = {} #the features dictionary of the form {mention_id: feature_vector} -- will use to create matrix 
        self.k = k #the number of bags we will randomly sample before using GP
        self.gp = GaussianProcessRegressor()
        feature_vector = ex.read_pickle_sparse()
        idx = 0 #the index for population mention2Index
        #the following will populate the labels_matrix and the features_dictionary
        for product in self.positiveProductMap.keys():
            for pair in self.positiveProductMap[product]:
                mentions = self.bag2Mentions[product][pair]
                if len(mentions) == 1:
                    self.bagsSizeOne.add((product, pair))
                for mentionID in mentions:
                    self.labels_matrix = np.append(self.labels_matrix, self.labels_dictionary[mentionID]) #append the value of the label to the labels matrix 
                    self.features_dictionary[mentionID] = feature_vector[mentionID]
                    self.mention2Index[mentionID] = idx
                    idx += 1
        self.features_matrix = np.array([v.toarray() for v in self.features_dictionary.values()]).reshape((len(self.features_dictionary.keys()), list(self.features_dictionary.values())[0].shape[1]))  #a matrix representation of the feature dictionary, where the mention_ids are replaced with a numerical index
        self.sentiment = [] #the number of positive mentions in a particular bag 
        self.total = [] #the number of mentions in a particular bag 
        '''
        Hyperparameter Beta
        '''
        
        self.beta = beta #exploration vs exploitation hyperparam
        
        '''
        Performance variables
        '''
        self.positives = 0 #the number of positive examples labeled 
        self.labeled_bags = 0 #number of bags labeled 
        self.labeled_mentions = 0 #number of mentions labeled 
       
        '''
        Prior mu and sigma
        '''
        self.mu = [0 for _ in range(self.features_matrix.shape[0])] #prior mu
        self.sigma = [0.5 for _ in range(self.features_matrix.shape[0])] #prior sigma 
        
        '''
        Model
        '''
      #  self.gp = GaussianProcessRegressor(alpha = 1e-5) #create gp object, add noise perturbation. Kernel is RBF by default.
        
        '''
        Features and labels
        '''
        self.X = None #initialize X to None. This will have the list of features
        self.T = np.array([]) #the labels of the features
        
        '''
        Flag
        '''
        self.xCreated = 0 #flag indicating whether we created the feature matrix 

    
    
    """
    learn() is called after each bag that is labeled.  This will continue to be called until there are no more bags.
    """
    
    def learn(self, iteration):
        #if we are less than k, we randomly sample from our set of bags of size 1
        if iteration < self.k:
            bag = random.sample(self.bagsSizeOne, 1) #randomly sample the bag 
            product = bag[0][0]
            pair = bag[0][1]
            mentions = self.bag2Mentions[product][pair] #all the mention_ids of the bag selected
            self.sample(mentions) #the sample method will add the feature and the label to self.X and self.T
            self.bagsSizeOne.remove((product, pair)) #remove this bag from the set 
            del self.bag2Mentions[product][pair] #ensure that we don't label the same bag again
            return product, pair
        self.mapBagsToScores() #update the mapping {product: (pair:average_score_of_mentions)}
        product, pair = self.argmax() #chose the (product, adj_noun_pair) that corresponds to the highest average score 
        mentions = self.bag2Mentions[product][pair] #all the mention_ids of the bag selected
        self.sample(mentions) #the sample method will add the feature and the label to self.X and self.T
        self.gp.fit(self.X, self.T)#This is what gets the Kernel Matrix (bottleneck)
        self.mu, self.sigma = self.gp.predict(self.features_matrix, return_std=True) #use gp.predict() to get new mu and sigma   
        del self.bag2Mentions[product][pair] #ensure that we don't label the same bag again
        self.bag2Scores = collections.defaultdict(dict)  #ensure that we don't label the same bag again
        return product, pair #return product,pair as the bag_id 
        
    """
    A function that maps the bag_id to the score of the mentions in this bag (the average of all the mentions).
    
    Format: [bag_id: average_score_of_mentions]
    
    """

    
    def mapBagsToScores(self):
        #the bag2Mentions dict is of the form: {product: (adj_noun_pair:mention_list)}
        #the outer two for loops represent the bag key, and the most inner for loop represents the mention_list
        for product in self.bag2Mentions.keys():
            for pair in self.bag2Mentions[product].keys(): #starting with one product now, will integrate them later 
                mention_list = self.bag2Mentions[product][pair] #mention_list stores all of the mention_ids for a particular bag
                total_score = 0 #used for average calculation
                num_mentions = 0 #used for num_mentions (can't simply use len(mention_list) since some mentions are not mapped in mention2Index, thus we are ignoring these mentions)
                for mention_id in mention_list:
                    #NOTE: Must use try/except since some mentions aren't mapped in mention2Index. We are ignoring these mentions
                    try:
                        i = self.mention2Index[mention_id] #get the index mapped by mention_id
                        total_score += (self.mu[i] + self.sigma[i]*np.sqrt(self.beta)) #increment total_score
                        num_mentions += 1 #increment the number of mentions we have (to be used in average calculation)
                    except:
                        continue
                        #print("mention not mapped in mention2Index...Continuing to next mention")
                #avoiding divide by 0 error.  We will not store this in bag2Scores if we have no mentions for [product][pair] key 
                if num_mentions > 0:
                    self.bag2Scores[product][pair] = total_score/num_mentions #store the average score for this bag_id in bag2Scores
            
            
            
        
    """
    This function will return the bag_id that maps to the highest value
    """    

    def argmax(self):
        max_val = -1 #initial maximum value is smaller than any possible max
        product_max = None
        pair_max = None
        for product in self.bag2Scores.keys(): #for each product
            for pair in self.bag2Scores[product].keys():
                if self.bag2Scores[product][pair] >= max_val:
                    max_val = self.bag2Scores[product][pair]
                    product_max = product
                    pair_max = pair
        return product_max, pair_max

   
    
    """
    add the features and labels of the selected bag to X and T, respectively
    """
    def sample(self, mentions):
        
        self.labeled_bags += 1 #increment the number of labeled bags by 1
        self.labeled_mentions += len(mentions)
        self.sentiment.append(0) #start with 0 positive mentions in this bag -- will update in the loop as we encounter positive mentions 
        self.total.append(len(mentions)) #add the total number of mentions in this bag
        indices = [self.mention2Index[mention] for mention in mentions] #the numerical indices since we cannot train a GP on a dictionary 
        for index in indices:
            try:
                self.T = np.append(self.T, self.labels_matrix[index]) #append the label
                if self.labels_matrix[index] == 1:  #increment the positive count if we have a positive label 
                    self.positives += 1
                    self.sentiment[-1] += 1 #we have a positive mention to add to this sentiment
                if self.xCreated == 0:
                    self.X = np.array(self.features_matrix[index,:]).reshape(1, -1)
                    self.xCreated = 1
                else:
                    self.X = np.vstack((self.X, (self.features_matrix[index,:])))  #append the feature vector 
            except Exception as e:
                print(e)


    
    
if __name__ == '__main__':
    
    initParm = dict(
        pair_mid = '../../data/five_product/data/raw/pair_mention/',
        id_file = '../../data/five_product/data/raw/id/',
        path_orisentence = "../../data/five_product/data/raw/mapping/mid_origin/",
        pair_label = '../../data/five_product/data/label/pairs_label/pair_label.json',
        cs_label = '../../data/five_product/data/label/mention_label/mention_label.json',
        ground_truth = '../../data/five_product/data/label/mention_label/ground_truth.json',
        path_pickle = "../../data/five_product/data/feature/new_pickle_sparse/",
        feature_json_path='../../data/five_product/data/feature/json_index/'
    )
    

    ex = data_extract(initParm)

    #k is the number of selections we will randomly pick 
    k = 10
    agent = GPUCB(ex, k) #create an agent
    
    #the following 2 lists are gong to append the number of labeled points and the proportion of positives, respectively, for plotting
    num_labeled_bags = []
    num_labeled_mentions = []
    precisions = []
    recalls = []
    f1s = []
    iteration = 0
    output_file = f"GPUCB Beta = {agent.beta}"
    out_f = open(output_file, 'w')
    while agent.labeled_bags < 200:
        product, pair = agent.learn(iteration) #we get the label from this iteration
        out_f.write(product + ";" + pair + "\n")
        print('Bag selected ({}, {}):  Proportion Positive: {}.  Num_Bags_Labeled: {}'.format(product, pair, agent.positives/agent.labeled_mentions, agent.labeled_bags, len(agent.labels_dictionary.keys())))
        num_labeled_bags.append(agent.labeled_bags)
        num_labeled_mentions.append(agent.labeled_mentions)
        precisions.append(agent.positives/float(agent.labeled_mentions))
        recalls.append(agent.positives/float(795))

        f1s.append(2 * precisions[-1] * recalls[-1] / (precisions[-1] + recalls[-1])) if (precisions[-1] + recalls[-1]) != 0 else f1s.append(0)
        print("F measure: ", f1s[-1])
        iteration += 1
    
    #write the sentiment and total to a file 
    with open('../../results/GPUCB.txt', 'w') as f:
        f.write("Sentiment: ")
        f.write(json.dumps(agent.sentiment))
        f.write("\n\nTotal: ")
        f.write(json.dumps(agent.total))
        f.write("\n\nF1 score: ")
        f.write(json.dumps(f1s))
        f.close()
       
    
    
    plt.plot(num_labeled_bags, f1s)
    plt.axis([0, agent.labeled_bags, 0, 1])
    plt.xlabel('Number of Bags Labeled')
    plt.ylabel('F1 score')
    plt.title('F score vs Number of Bags Labeled')
    plt.savefig('../../results/GPUCB.jpeg')  
    plt.close()


