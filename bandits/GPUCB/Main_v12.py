"""
Created on Sat Jun 22 18:04:36 2019

@author: scheb
"""

"""Code is based upon: https://github.com/tushuhei/gpucb/blob/master/gpucb.py """

#Next step: speed up with r1 update, try different kernels and different betas 


#IMPORTANT: WHERE TO READ LABEL DATA: 
#Use read_positive to get positive bag ids, then use mapping pair2mentionID to get mentions, and then use read_ground_truth to get label of the mentions 
#  Use the return dictionary of read_ground_truth to get the label
#  Linear kernel (but most important is to use rank-one-update)
import sys, os
sys.path.append("/home/jakob/Desktop/metabandit/repo/bandit_crowdsourcing/data/")

#from mymodule import MyModule
import numpy as np
#from mpl_toolkits.mplot3d import Axes3D
#from matplotlib import pylab as plt
import matplotlib.pyplot as plt
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
from scipy.spatial.distance import pdist, squareform
from data_extract import *
from sklearn.metrics.pairwise import rbf_kernel

import scipy
import json
import random
import collections



class GPUCB(object):

    def __init__(self, ex, k, aggregation_func, clustering, beta=100.):
        '''
        ex: an object from data_extract.py that has access to methods to read the dataset
        
        beta (optional): Hyper-parameter to tune the exploration-exploitation
        balance. If beta is large, it emphasizes the variance of the unexplored
        solution solution (i.e. larger curiosity).

        This constructor intializes the following:

          positiveProductMap: a map of the product name to the set of adj/noun pairs
          bag2Mentions: a dictionary of bagIDs to mention lists in that bag
          bagsSizeOne: a se4t of bag IDs that are size 1
          testing_labels_dictionary: a dictionary containing mentionID maps to labels -- the set of labels to score
          training_labels_dictionary: incremental learning dictionary that stores labels to train on iteratively
        
        '''
        
        
        '''
        Dictionaries 
        '''
        self.positiveProductMap = ex.read_positive() #format: {prod_name: [pos_pair1, pos_pair2, ... ]}    

        self.bag2Mentions = ex.read_pair_mention() #a dictionary of the form {bag_id: mention_list}
        
        self.bagsSizeOne = set()#the collection of bags that are size 1
        self.testing_labels_dictionary = ex.get_ppm_truth() #a dictionary of the form {mention_id: label}
        self.training_labels_dictionary = {}
        self.testing_features_dictionary = {} #the features dictionary of the form {mention_id: feature_vector} -- will use to create matrix 
        self.training_features_dictionary = {} #the features extracted that will be iteravely used for training 
       

        self.aggregation_func = aggregation_func
        self.mentions2Scores = {} # a map [mentionID:score]
        self.Cinv = np.array([1]) # initialize the Cinv to 1, since this is the kernel of 1 feature with itself 
        feature_vector = ex.read_pickle_sparse()
        self.k = k
        self.bigbags = []
        #the following will populate the labels_matrix and the features_dictionary with the POSITIVE features 
        for product in self.positiveProductMap.keys():
            for pair in self.positiveProductMap[product]:
                mentions = self.bag2Mentions[product][pair]
                self.bigbags += [(product,pair,len(mentions))]
                if len(mentions) == 1:
                    self.bagsSizeOne.add((product, pair))
                for mentionID in mentions:
                    self.testing_features_dictionary[mentionID] = feature_vector[mentionID]
        self.bigbags = sorted(self.bigbags,key=lambda a:a[2],reverse=True)[:k] 

        self.training_features_matrix = np.array([])
        self.testing_features_matrix = np.array([v.toarray() for v in self.testing_features_dictionary.values()]).reshape((len(self.testing_features_dictionary.keys()), list(self.testing_features_dictionary.values())[0].shape[1]))  #a matrix representation of the feature dictionary, where the mention_ids are replaced with a numerical index
        self.sentiment = [] #the number of positive mentions in a particular bag 
        self.total = [] #the number of mentions in a particular bag 
        '''
        Hyperparameter Beta
        '''
        self.bags_used = set() 
        self.beta = beta #exploration vs exploitation hyperparam
        '''
        Performance variables
        '''
        self.positives = 0 #the number of positive examples labeled 
        self.labeled_bags = 0 #number of bags labeled 
        self.labeled_mentions = 0 #number of mentions labeled 
        self.clustering = clustering
        


    '''
    A helper method to convert a dictionary to a matrix, where each row is a value in the dictionary
    Was not used 
    '''
    def dict2Matrix(self, dictionary):
      temp = np.array([])
      tempStarted = 0
      for key in dictionary.keys():
        if tempStarted == 0:
          tempStarted = 1
          temp = dictionary[key].toarray()
        else:
          temp = np.vstack((temp, dictionary[key].toarray()))
        
      return temp
    '''
    The kernel function we are using
    Assume inputs are m x n where there are m training examples and n features
    '''
    def RBF_kernel(self, trainingFeatures, testingFeatures):
       return rbf_kernel(trainingFeatures, testingFeatures)
    

    '''
    Update the mentions2Scores dictionary
    Invariant: the self.Cinv is always updated 
    Called at every iteration of learn()
    '''

    def scoreMentions(self):
        #We score the mentions in testingFeatures using the training features and labels 
        trainingFeatures = self.training_features_matrix
        trainingLabels =  np.array(list(self.training_labels_dictionary.values()))
        testingFeatures = self.testing_features_matrix
        k = self.RBF_kernel(trainingFeatures, testingFeatures)
        k_transp = k.T
        #mu should be an mx1 vector where m is the number of testing features
        mu = ((k_transp.dot(self.Cinv)).dot(trainingLabels))
        #c should be mx1
        c = np.diag(self.RBF_kernel(testingFeatures, testingFeatures))

        sigma = c - np.diag((k_transp.dot(self.Cinv)).dot(k))
        #for each mention in the testing mentions, score them
        for idx, mention in enumerate(self.testing_features_dictionary.keys()):
            
            weight = 1.0
            if self.clustering=="weighted":
                weight = self.cluster_map.get(mention,1.0)
            self.mentions2Scores[mention] = (mu[idx] + sigma[idx] * np.sqrt(self.beta))*weight

    '''
    Returns the highest scoring bag 
    '''
    def getHighestScoringBag(self):
        #the bag2Mentions dict is of the form: {product: (adj_noun_pair:mention_list)}
        #the outer two for loops represent the bag key, and the most inner for loop represents the mention_list
        maxScore = -np.inf
        retProd = None
        retPair = None

        for product in self.bag2Mentions.keys():
            for pair in self.bag2Mentions[product].keys(): #starting with one product now, will integrate them later 
                if product+";"+pair in self.bags_used: #avoid repeats in metabandit
                    continue
                #we now have a bag with a mention list
                mention_list = self.bag2Mentions[product][pair] #mention_list stores all of the mention_ids for a particular bag
                if len(mention_list) == 0:
                    del self.bag2Mentions[product][pair]
                    break
                score = []
                flag = False
                for mention in mention_list:
                    #if we have not scored the mention
                    if mention not in self.mentions2Scores.keys():
                        flag = True
                        break
                    score += [self.mentions2Scores[mention]]
                if flag:
                    continue

                score = self.aggregation_func(score) #this is the avg score 
                if score >= maxScore:
                    maxScore = score
                    retProd = product
                    retPair = pair 
        return retProd, retPair


    
    """
    learn() is called after each bag that is labeled.  This will continue to be called until there are no more bags.
    """
    

    def learn(self, i):
        #if we are less than k, we randomly sample from our set of bags of size 1
        if i < self.k:
            bag = self.bigbags[i] #randomly sample the bag 
            product = bag[0]
            pair = bag[1]

            mentions = self.bag2Mentions[product][pair] #all the mention_ids of the bag selected
            self.sample(mentions) #the sample method will add the feature and the label to self.X and self.T
            del self.bag2Mentions[product][pair] #ensure that we don't label the same bag again
            self.bags_used.add(product+";"+pair)
            return product, pair
        self.scoreMentions()
        product, pair = self.getHighestScoringBag() 
        mentions = self.bag2Mentions[product][pair] #all the mention_ids of the bag selected
        self.sample(mentions) #the sample method will add the feature and the label to self.X and self.T
        del self.bag2Mentions[product][pair] #ensure that we don't label the same bag again
        return product, pair #return product,pair as the bag_id 
        

    def r1_update(self, mention):
        newFeature = (self.testing_features_dictionary[mention]).toarray()
        #c is the kernel function k(xN+1, xN+1)
        c = self.RBF_kernel(newFeature, newFeature)
        b = self.RBF_kernel(self.training_features_matrix , newFeature)
        bT = b.T

        k = c - (bT.dot(self.Cinv)).dot(b)


        topLeft = self.Cinv + (1/k) * ((self.Cinv.dot(b)).dot(bT)).dot(self.Cinv)
        topRight = -(1/k) * self.Cinv.dot(b)
        bottomLeft = -(1/k) * bT.dot(self.Cinv)
        bottomRight = 1/k

        topRow = np.hstack((topLeft, topRight))
        bottomRow = np.hstack((bottomLeft, bottomRight))
        self.Cinv = np.vstack((topRow, bottomRow))

    """
    add the features and labels of the selected bag to X and T, respectively

    @param mentions is the mentions of the bag that we are adding to the training set
    """
    def sample(self, mentions):
        
        self.labeled_bags += 1 #increment the number of labeled bags by 1
        self.labeled_mentions += len(mentions)
        self.sentiment.append(0) #start with 0 positive mentions in this bag -- will update in the loop as we encounter positive mentions 
        self.total.append(len(mentions)) #add the total number of mentions in this bag
      
        #for every mention passed in, add it to the training set of mentions, the training labels set, and remove it from the set of features we are trying to predict 
        for mention in mentions:
            if self.testing_labels_dictionary[mention] == 1:
                self.positives += 1
                self.sentiment[-1] += 1 #we have a positive mention to add to this sentiment


            '''
            PERFORM RANK ONE UDPATE (only if we have more than one training point )
            '''
            #the index of the testing features to remove 
            indexToRem = list(self.testing_features_dictionary.keys()).index(mention)
            if len(self.training_features_dictionary) < 1:
              #we have sampled the first training feature 
              self.training_features_matrix = self.testing_features_dictionary[mention].toarray()
              #assign the mention to training features dict
              self.training_features_dictionary[mention] = self.testing_features_dictionary[mention]
              #delete the mention from testing features dictionary 
              del self.testing_features_dictionary[mention]
              #delete from testing features matrix 
              self.testing_features_matrix = np.delete(self.testing_features_matrix, (indexToRem), axis=0)
              #take the label into training labels
              self.training_labels_dictionary[mention] = self.testing_labels_dictionary[mention]
              #remove the label from testing labels 
              del self.testing_labels_dictionary[mention]
              continue
            #we now need to perform the rank one update
            #rank one update source: http://www.cs.nthu.edu.tw/~jang/book/addenda/matinv/matinv/
            
            self.r1_update(mention)
            #this is the new feature we are adding to our collection
            #this is a row vector 
            
            newFeature = (self.testing_features_dictionary[mention]).toarray()

            #update the training & testing matrices 
            self.training_features_matrix = np.vstack((self.training_features_matrix, newFeature))
            self.training_features_dictionary[mention] = self.testing_features_dictionary[mention]
            del self.testing_features_dictionary[mention]
            self.training_labels_dictionary[mention] = self.testing_labels_dictionary[mention]
            del self.testing_labels_dictionary[mention]
            self.testing_features_matrix = np.delete(self.testing_features_matrix, (indexToRem), axis=0)

    
 
if __name__ == '__main__':
    
    initParm = dict(
        pair_mid = '../../../deep-mire/data/raw/pair_mention/',
        id_file = '../../../deep-mire/data/raw/id/',
        path_orisentence = "../../../deep-mire/data/raw/mapping/mid_origin/",
        pair_label = '../../../deep-mire/data/label/pairs_label/pair_label.json',
        cs_label = '../../../deep-mire/data/label/mention_label/mention_label.json',
        ground_truth = '../../../deep-mire/data/label/mention_label/ground_truth.json',
        path_pickle = "../../../deep-mire/data/feature/new_pickle_sparse/",
        feature_json_path='../../../deep-mire/data/feature/json_index/'
    )
    

    ex = data_extract(initParm)
    sara = ex #laooooooooo
    agents = [(GPUCB(ex, 3, b[1],a),a,b[0]) for a in [25,50,100,200,400] for b in [("min",lambda a:np.min(a)),("avg",lambda a:np.mean(a)),("max",lambda a:np.max(a))]]  #create an agent
    for agent in agents:
        ffff = open("GP-arm-%f-%s.csv"%(agent[1],agent[2]),"w+")
        print("Iteration,Precision,Recall",file=ffff)
        for i in range(33):
            agent[0].learn(i)
            if i == 2:
                agent[0].positives = 0
                agent[0].labelled_mentions = 0
            s="%d,%f,%f"%(i-3,agent[0].positives/float(agent[0].labeled_mentions),agent[0].positives/795.0)
            if i > 2:
                print(s)
                print(s,file=ffff)
        exit(0)
    '''
    np.seterr(all='raise') 
    #the following 2 lists are gong to append the number of labeled points and the proportion of positives, respectively, for plotting
    num_labeled_bags = []
    num_labeled_mentions = []
    precisions = []
    recalls = []
    f1s = []
    iteration =    output_file = f"GPUCB Beta = {agent.beta}"
    out_f = open("../../results/" + output_file, 'w')
    while agent.labeled_bags < 300:
        product, pair = agent.learn(iteration) #we get the label from this iteration
        out_f.write(product + ";" + pair + "\n")
        print('Bag selected ({}, {}):  Proportion Positive: {}.  Num_Bags_Labeled: {}'.format(product, pair, agent.positives/agent.labeled_mentions, agent.labeled_bags, agent.labeled_bags))    
        num_labeled_bags.append(agent.labeled_bags)
        num_labeled_mentions.append(agent.labeled_mentions)
        precisions.append(agent.positives/float(agent.labeled_mentions))
        recalls.append(agent.positives/float(795))

        f1s.append(2 * precisions[-1] * recalls[-1] / (precisions[-1] + recalls[-1])) if (precisions[-1] + recalls[-1]) != 0 else f1s.append(0)
        print("Precision: ", precisions[-1])
        print("Recall: ", recalls[-1])
        iteration += 1
    
    #write the sentiment and total to a file 
    with open('../../results/GPUCB.txt', 'w') as f:
        f.write("Sentiment: ")
        f.write(json.dumps(agent.sentiment))
        f.write("\n\nTotal: ")
        f.write(json.dumps(agent.total))
        f.write("\n\nPrecision: ")
        f.write(json.dumps(precisions))
        f.write("\n\nRecalls: ")
        f.write(json.dumps(recalls))
        f.write("\n\nF1 score: ")
        f.write(json.dumps(f1s))
        f.close()
       
    
    
    plt.plot(num_labeled_bags, f1s)
    plt.axis([0, agent.labeled_bags, 0, 1])
    plt.xlabel('Number of Bags Labeled')
    plt.ylabel('F1 score')
    plt.title('F score vs Number of Bags Labeled')
    plt.savefig('../../results/GPUCB.jpeg')  
    plt.close()
    '''
