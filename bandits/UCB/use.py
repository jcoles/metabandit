from clustering import *
from UCB import *

import sys
sys.path.insert(0, '../../graphical_model/MRF')
from in_out import *
sys.path.insert(0, '../../data/')
from data_extract import *

import pickle

class use:
	def gene_dic(edges):
		edges_dic = {}
		for k, v in edges.items():
			mention_id_1, mention_id_2 = k.split('-')
			if edges_dic.get(mention_id_1) != None:
				edges_dic[mention_id_1].append(mention_id_2)
			if edges_dic.get(mention_id_1) == None:     # this mention does not have edge yet
				edges_dic[mention_id_1] = []
				edges_dic[mention_id_1].append(mention_id_2)
			if edges_dic.get(mention_id_2) != None:
				edges_dic[mention_id_2].append(mention_id_1)
			if edges_dic.get(mention_id_2) == None:     # this mention does not have edge yet
				edges_dic[mention_id_2] = []
				edges_dic[mention_id_2].append(mention_id_1)
		return edges_dic
	
	if __name__ == '__main__':
		initParm = dict(
			pair_mid='../../data/data/raw/pair_mention/',
			id_file='../../data/data/raw/id/',
			path_orisentence="../../data/data/raw/mapping/mid_origin/",
			pair_label='../../data/data/label/pairs_label/pair_label.json',
			cs_label='../../data/data/label/mention_label/ground_truth.json',
			ground_truth='../../data/data/label/mention_label/ground_truth.json',
			path_pickle="../../data/data/feature/new_pickle_sparse/",
			feature_json_path='../../data/data/feature/new_json_index/'
		)

		edges = loadpickle("../../data/clean_data/edges.pkl")
		ground_truth = load_json("../../data/data/label/mention_label/ground_truth.json")
		edges_dic = gene_dic(edges)
		
		json_object = json.dumps(edges_dic, indent = 4) 
		
		with open("edges_dic.json", "w") as outfile: 
			outfile.write(json_object) 
		
		
		G = nx.Graph()
		for key, value in edges_dic.items():
			for i in range(len(value)):
				G.add_edge(key, value[i])
		groups, pos, arms_recall = clustering.part_graph(G, 20, ground_truth)

		
		ex = data_extract(initParm)
		bag_mentions = ex.read_pair_mention()
		y_dict = ex.get_all_truth()
		bag_labels = {}
		empty_bags = []
		
		for product, bag_dict in bag_mentions.items():
			for bag, mention_list in bag_dict.items():
				bag_id = product + ';' + bag
				
				bag_labels[bag_id] = 0
				
				if len(mention_list) == 0:
					empty_bags.append(bag_id)
					continue
			
				for m_id in mention_list:
					if y_dict[m_id] == 1:
						bag_labels[bag_id] = 1
				
		annotated_bags = UCB.UCB_iteration(groups, pos, arms_recall, bag_labels, bag_mentions, empty_bags)
		