import numpy as np
import pandas as pd

import math

# Implementing UCB
# http://www.aionlinecourse.com/tutorial/machine-learning/upper-confidence-bound-%28ucb%29
class UCB:
    '''
    return a list with the number of time each subgraph is selected
    sample printout: num of sel [705, 387, 186, 345, 6323, 150, 292, 1170, 256, 186]
                     sum of rewards [120, 47, 7, 38, 1675, 1, 27, 236, 20, 7]
    '''
    def UCB_alg(groups, pos):
        # d: num of subgraphs
        # N: num of rounds
        d = len(groups)
        N = min(map(len, groups))
        print('d', d, 'N', N)
        mentions_selected = []
        numbers_of_selections = [0] * d
        sums_of_rewards = [0] * d
        total_reward = 0
        for n in range(0, N):
            index = 0
            max_upper_bound = 0
            for i in range(0, d):
                if (numbers_of_selections[i] > 0):      # each selected at least once before entering into the main algorithm
                    average_reward = sums_of_rewards[i] / numbers_of_selections[i]
                    delta_i = math.sqrt(3/2 * math.log(n + 1) / numbers_of_selections[i])
                    upper_bound = average_reward + delta_i
                else:
                    upper_bound = 1e400
                if upper_bound > max_upper_bound:
                    max_upper_bound = upper_bound
                    index = i
            mentions_selected.append(index)
            numbers_of_selections[index] = numbers_of_selections[index] + 1
            reward = pos[index][n] # get the reward score for this mention
            
            sums_of_rewards[index] = sums_of_rewards[index] + reward
            total_reward = total_reward + reward

        print('num of sel', numbers_of_selections)
        print('sum of rewards', sums_of_rewards)

    def evalute(total_pos, total_annotated, num_pos_mentions):
        precision, recall = [], []
        precision.append(total_pos / float(total_annotated))
        recall.append(total_pos / float(num_pos_mentions))
        return precision, recall