# necessary install: pip install metis
# Turn it on by issuing: make config shared=1

import networkx as nx
import metis
import json

class clustering:
	def part_graph(G, n, ground_truth):
		# n: clustering in to n groups
		# G: graph structure (networkx)
		print('len nodes', len(G.nodes()))
		
		(edgecuts, parts) = metis.part_graph(G, n)
		
		# separate into n lists, each contains index of the ones in same group 
		groups = []
		pos = []
		arms_recall = []
		
		for i in range(n):
			groups.append([])
			pos.append([])
		for i, p in enumerate(parts):
			mention_id = G.nodes()[i]
			groups[p].append(mention_id)
			pos[p].append(ground_truth[mention_id])
		
		
		for i in range(n):
			arms_recall.append(sum(pos[i])/len(groups[i]))  #number of labeled positive mentions / number of labeled mentions
			print('group',i, 'len of components', len(groups[i]), 'num of positives', sum(pos[i]), 'recall score', arms_recall[i])
		
		json_object = json.dumps(groups, indent = 4) 
		
		with open("sample.json", "w") as outfile: 
			outfile.write(json_object) 
		
		return groups, pos, arms_recall
	