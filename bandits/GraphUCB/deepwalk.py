# https://github.com/shenweichen/GraphEmbedding
from csv import writer
import numpy as np
import networkx as nx
import pickle
import json 
from sklearn.cluster import KMeans

from ge.classify import read_node_label, Classifier
from ge import DeepWalk
from sklearn.linear_model import LogisticRegression

import sys
sys.path.insert(0, '../../graphical_model/MRF')
from in_out import *
sys.path.insert(0, '../../data/')
from data_extract import *
from UCB import *

# only called once to generate that file
def generate_edgelist():
    edges = loadpickle("../../data/clean_data/edges.pkl")
    ground_truth = load_json("../../data/data/label/mention_label/ground_truth.json")

    edges_dic = {}
    edges_txt = ""
    for k, v in edges.items():
        mention_id_1, mention_id_2 = k.split('-')
        edges_txt += mention_id_1.replace(" ", "")
        edges_txt += " "
        edges_txt += mention_id_2.replace(" ", "")
        edges_txt += "\n"
    
    file1 = open("../data/edgelist.txt","w") 
    file1.write(edges_txt)
    file1.close()

def kmeans(X, num_clusters):
    #X = np.array([[1, 2], [1, 4], [1, 0], [10, 2], [10, 4], [10, 0]])
    kmeans = KMeans(n_clusters=num_clusters, random_state=0).fit(X)
    #print(kmeans.labels_)
    #kmeans.predict([[0, 0], [12, 3]])
    return kmeans.labels_

def append_list_as_row(file_name, list_of_elem):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)

if __name__ == "__main__":
    initParm = dict(
        pair_mid='../../data/data/raw/pair_mention/',
        id_file='../../data/data/raw/id/',
        path_orisentence="../../data/data/raw/mapping/mid_origin/",
        pair_label='../../data/data/label/pairs_label/pair_label.json',
        cs_label='../../data/data/label/mention_label/ground_truth.json',
        ground_truth='../../data/data/label/mention_label/ground_truth.json',
        path_pickle="../../data/data/feature/new_pickle_sparse/",
        feature_json_path='../../data/data/feature/new_json_index/'
    )
    ground_truth = load_json("../../data/data/label/mention_label/ground_truth.json")

    # mention without space match with original mention
    matching = {}
    for key in ground_truth.keys():
        no_space = key.replace(" ", "")
        matching[no_space] = key
    
    '''
    # check the three models (best, median, worst recall score)
    iter_list = [3,5,10,15,20]
    walk_list = [3,4,7,10]
    method_list = ["avg", "max", "min"]
    num_groups = 20
    iter_num = 0

    recall_list = []
    final_recall_list = {}
    for iteration in iter_list:
        for walk_length in walk_list:
            for method in method_list:
                #generate_edgelist()
                G = nx.read_edgelist('../data/edgelist.txt',create_using=nx.DiGraph(),nodetype=None,data=[('weight',int)])# Read graph

                model = DeepWalk(G,walk_length=walk_length,num_walks=80,workers=1)#init model  walk_length: how far away two mentions can be related; smaller 3,5,7
                model.train(window_size=5,iter=iteration)# train model change iter = 10; 15
                embeddings = model.get_embeddings()# get embedding vectors
                
                # use kmeans clustering
                mentions_nospace = list(embeddings.keys())
                X = list(embeddings.values())
                #print(len(X), len(X[0]))

                # turn mentions back to including space version
                mentions = []
                for i in range(len(mentions_nospace)):
                    new = matching[mentions_nospace[i]]
                    mentions.append(new)

                labels = kmeans(X, num_groups)

                cluster_result = {}
                for i in range(0, len(labels)):
                    cluster_result[mentions[i]] = int(labels[i])
                print(cluster_result)
            
                
                # separate into n lists, each contains index of the ones in same group 
                groups = []
                total_pos = 0
                for i in range(num_groups):
                    groups.append([])
                
                for i in range(0, len(labels)):
                    mention_id = mentions[i]
                    p = int(labels[i])
                    groups[p].append(mention_id)
                    total_pos += ground_truth[mention_id]
                
                ex = data_extract(initParm)
                bag_mentions = ex.read_pair_mention()
                y_dict = ex.get_all_truth()
                bag_labels = {}
                empty_bags = []
                
                for product, bag_dict in bag_mentions.items():
                    for bag, mention_list in bag_dict.items():
                        bag_id = product + ';' + bag
                        
                        bag_labels[bag_id] = 0
                        
                        if len(mention_list) == 0:
                            empty_bags.append(bag_id)
                            continue
                    
                        for m_id in mention_list:
                            if y_dict[m_id] == 1:
                                bag_labels[bag_id] = 1
                

                annotated_bags, recalls, precisions = UCB.UCB_iteration(groups,total_pos, bag_labels, bag_mentions, empty_bags, ground_truth, method, cluster_result)
                #print(annotated_bags)
                arms = "Graph-" + method + "-iter" + str(iteration) + "-walk" + str(walk_length)
                print(arms)

                recall_list.append(recalls[-1])
                final_recall_list[arms] = []
                final_recall_list[arms].append(recalls[-1])
                final_recall_list[arms].append(method)
                final_recall_list[arms].append(iteration)
                final_recall_list[arms].append(walk_length)
    print(final_recall_list)

    three_index = []
    three_index.append(recall_list.index(max(recall_list)))
    three_index.append(recall_list.index(min(recall_list)))
    three_index.append(recall_list.index(np.percentile(recall_list,50,interpolation='nearest')))
    print(three_index)
    '''
    
    # write to csv file the three models result
    new_iter_list = [10, 15, 15]
    new_walk_list = [3, 3, 4]
    new_method_list = ["max", "max", "avg"]
    num_groups = 20
    iter_num = 0


    with open('graph_result.csv', 'w', newline='') as file:
        csv_writer = writer(file)
        csv_writer.writerow(["Iteration", "Arm", "Recall", "Precision", "Bagid"])

    for i in range(0, len(new_iter_list)):
        iteration = new_iter_list[i]
        walk_length = new_walk_list[i]
        method = new_method_list[i]

        #generate_edgelist()
        G = nx.read_edgelist('../data/edgelist.txt',create_using=nx.DiGraph(),nodetype=None,data=[('weight',int)])# Read graph

        model = DeepWalk(G,walk_length=walk_length,num_walks=80,workers=1)#init model  walk_length: how far away two mentions can be related; smaller 3,5,7
        model.train(window_size=5,iter=iteration)# train model change iter = 10; 15
        embeddings = model.get_embeddings()# get embedding vectors
        
        # use kmeans clustering
        mentions_nospace = list(embeddings.keys())
        X = list(embeddings.values())
        #print(len(X), len(X[0]))

        # turn mentions back to including space version
        mentions = []
        for i in range(len(mentions_nospace)):
            new = matching[mentions_nospace[i]]
            mentions.append(new)

        labels = kmeans(X, num_groups)

        cluster_result = {}
        for i in range(0, len(labels)):
            cluster_result[mentions[i]] = int(labels[i])
        #print(cluster_result)
        
        # separate into n lists, each contains index of the ones in same group 
        groups = []
        total_pos = 0
        for i in range(num_groups):
            groups.append([])
        
        for i in range(0, len(labels)):
            mention_id = mentions[i]
            p = int(labels[i])
            groups[p].append(mention_id)
            total_pos += ground_truth[mention_id]
        
        ex = data_extract(initParm)
        bag_mentions = ex.read_pair_mention()
        y_dict = ex.get_all_truth()
        bag_labels = {}
        empty_bags = []
        
        for product, bag_dict in bag_mentions.items():
            for bag, mention_list in bag_dict.items():
                bag_id = product + ';' + bag
                
                bag_labels[bag_id] = 0
                
                if len(mention_list) == 0:
                    empty_bags.append(bag_id)
                    continue
            
                for m_id in mention_list:
                    if y_dict[m_id] == 1:
                        bag_labels[bag_id] = 1
        

        annotated_bags, recalls, precisions = UCB.UCB_iteration(groups, total_pos, bag_labels, bag_mentions, empty_bags, ground_truth, method, cluster_result)
        #print(annotated_bags)
        arms = "Graph-" + method + "-iter" + str(iteration) + "-walk" + str(walk_length)
        print(arms)

        # write to csv 
        # format: 
        # iteration, Arm, Recall, Precision, Bagid 
        # 0, Graph-avg-1, 
        # 1, Graph-sum-1,
        iter_num = 0
        for i in range(0, len(annotated_bags)):
            iter_num += 1
            #print(recalls[i], precisions[i], id)
            row_contents = [iter_num, arms, recalls[i], precisions[i], annotated_bags[i]]
            append_list_as_row('graph_result.csv', row_contents)
    
    '''
    write_string = ""
    for x in annotated_bags:
        write_string += x
        write_string += "\n"
    filename = "annotate/" + "cluster_iter" + str(iteration) + "_walk" + str(walk_length) + ".txt"
    file = open(filename,"w") 
    file.write(write_string)
    file.close()
    '''
    