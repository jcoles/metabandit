import numpy as np
import pandas as pd
import json
import math

# Implementing UCB
# http://www.aionlinecourse.com/tutorial/machine-learning/upper-confidence-bound-%28ucb%29
class UCB:
    def __init__(self, cluster_path, total_pos, bag_labels, bag_mentions, empty_graph, aggregation_func,ground_truth):
        with open(cluster_path,"r") as cluster_path_file:
            self.cluster_result = json.load(cluster_path_file)
        
        
        
        self.total_pos = total_pos
        self.bag_labels = bag_labels
        self.bag_mentions = bag_mentions
        self.empty_bags = empty_graph
        self.aggregation_func = aggregation_func
        with open(ground_truth,"r") as ground_truth_file:
            self.ground_truth = json.load(ground_truth_file)

        self.len_groups = len(set(self.cluster_result.values()))
        
        self.groups = {i:[] for i in range(self.len_groups)}
        for m in self.cluster_result:
            self.groups[self.cluster_result[m]] += [m]
        self.num_selections = [0]*self.len_groups
        self.annotated_bags = set()
        self.recalls = []
        self.precisions = []
        self.all_selected_mention = []
        self.groups_add = [[] for i in range(self.len_groups)]
        self.pos_add = [[] for i in range(self.len_groups)]
        self.arms_recall = [0 for i in range(self.len_groups)]
        
        self.bag_scores = []
        self.bag_len = {}
        for bag_id, bag_label in bag_labels.items():
            product, pair = bag_id.split(';')
            mention_list = self.bag_mentions[product][pair]
            self.bag_len[bag_id] = len(mention_list)

        
    def UCB_one(self, m_id, time_step):
        # num_selections: a list with number of selected mentions in each cluster group
        reward = 0
        for i in range(self.len_groups):
            if m_id in self.groups[i]:
                recall_score = self.arms_recall[i] # recall score for the clsutering group containing this mention
                if self.num_selections[i] == 0:
                    reward = recall_score
                else:
                    reward = recall_score + math.sqrt(2*math.log(time_step)/self.num_selections[i])
        return reward
    
    # total_pos: total num of positive mentions in dataset
    def UCB_iteration(self,t,initial_k):
        selected_bag_id = None
        if initial_k:
            bag_id_sorted = sorted(self.bag_len, key=lambda k: self.bag_len[k], reverse=True)
            print(bag_id_sorted)
            selected_bag_id = bag_id_sorted[t]
            self.annotated_bags.add(selected_bag_id)
        else:
            bag_scores = []
            for bag_id, bag_label in self.bag_labels.items():
                    # skip the annotated or empty bags
                    if bag_id in self.annotated_bags or bag_id in self.empty_bags:
                        continue

                    product, pair = bag_id.split(';')
                    mention_list = self.bag_mentions[product][pair]
                    
                    all_scores = []
                    for m_id in mention_list:
                        all_scores.append(self.UCB_one(m_id, t))

                    bag_score = self.aggregation_func(np.array(all_scores))
                    bag_scores.append((bag_id, bag_score))
            bag_scores = sorted(bag_scores, reverse = True, key = lambda x:x[1])

            selected_bag_id = bag_scores[0][0]
            self.annotated_bags.add(selected_bag_id)
        arms_recall2 = []
        product, pair = selected_bag_id.split(';')
        selected_mention_list = self.bag_mentions[product][pair]
        for mention in selected_mention_list:
            try:
                p = int(self.cluster_result[mention])
                self.groups_add[p].append(mention)
                self.pos_add[p].append(self.ground_truth[mention])
            except KeyError:
                print('key error cluster', mention)
        for i in range(self.len_groups):
            if len(self.groups_add[i]) > 0:
                arms_recall2.append(sum(self.pos_add[i])/len(self.groups_add[i])) 
            else:
                arms_recall2.append(0)
        self.arms_recall = arms_recall2
        return selected_bag_id
