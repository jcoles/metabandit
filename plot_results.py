from sys import argv
import matplotlib.pyplot as plt

metabandit_to_plot = sys.argv[1]
tmp = metabandit_to_plot[11:-4].split("-")
for i in range(len(tmp)/3):
    base_type = tmp[i*3]
    base_agg = tmp[i*3+1]
    base_hyper = tmp[i*3+2]
