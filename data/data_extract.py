import json
import os
import pickle
import numpy as np


class data_extract:
    def __init__(self, initParm):
        self.path_pair_mid = initParm['pair_mid']
        self.path_id = initParm['id_file']
        self.product_list = [product.split('_id', 1)[0] for product in os.listdir(self.path_id)]
        self.path_label = initParm['pair_label']
        self.path_cs_label = initParm['cs_label']
        self.path_ground_truth = initParm['ground_truth']
        self.path_mid_orisent = initParm['path_orisentence']
        self.path_pickle = initParm['path_pickle']
        self.path_feature_json = initParm['feature_json_path']

        # all methods 
        # self.pair_mention = self.read_pair_mention()
        # self.positive_pair = self.read_positive()
        # self.review = self.read_review()
        # self.cs_label = self.read_cs_label()
        # self.mid_origin = self.read_mid_origin()
        # self.feature_vector = self.read_pickle_sparse()
        # self.json_index = self.read_json_index()
        # self.get_all_truth()
        # self.get_ppm_truth()

    # one way to evaluate result: (get ground truth for both positive and negative pair mentions)
    def get_all_truth(self):
        """
            format: { mention_id : label(0/1) }
            all final label results (human annotated positive pairs + preprocessed negative pairs)
        """
        # get manually annotate results
        ground_truth = {}
        with open(self.path_ground_truth)as json_file:
            ground_truth = json.load(json_file)

        # get all mentions
        all_mention_gt = {}
        for product in self.product_list:
            with open(self.path_pair_mid + product + '.json', 'r') as pair_mid_file:
                pair_mid = json.load(pair_mid_file)
                for bag, mention_list in pair_mid.items():
                    for i in range(len(mention_list)):
                        try:
                            all_mention_gt[mention_list[i]] = ground_truth[mention_list[i]]
                        except KeyError:  # if not exist in ground truth, it is a negative pair
                            all_mention_gt[mention_list[i]] = 0
        return all_mention_gt

    # another way to evaluate result: (get ground truth for only positive pair mentions)
    def get_ppm_truth(self):
        """
            format: { mention_id : label(0/1) }
            all final label results (human annotation)
        """
        ground_truth = {}
        with open(self.path_ground_truth)as json_file:
            ground_truth = json.load(json_file)
        return ground_truth

    def read_pair_mention(self):
        # /home/jakob/samesh/repo/bandit_crowdsourcing/data\raw/pair_mention/*.json
        """
            format: { 'bag_id' :
                        ['mention_id', 'mention_id', ...],
                       ...
                    }
            mapping between bags and mentions
        """
        pair_mention = dict()
        for product in self.product_list:
            with open(self.path_pair_mid + product + '.json', 'r') as pair_mid_file:
                pair_mention[product] = json.load(pair_mid_file)
        return pair_mention

    def read_review(self):
        # /home/jakob/samesh/repo/bandit_crowdsourcing/data\raw/id/*_id.txt
        """
            format: {'product name': 
                        {'sentence_id': 'sentence', ...},
                      ...
                    }
        """
        review = dict()
        for product in self.positive_pair:
            review[product] = dict()
            with open(self.path_id + product + '_id.txt', 'r') as review_file:
                for lines in review_file:
                    review[product][lines.split(' ', 1)[0]] = lines.split(' ', 1)[1]
        return review

    def read_mid_origin(self):
        # /home/jakob/samesh/repo/bandit_crowdsourcing/data\raw/mapping/mid_origin/*.json
        """
            format: { mention_id1 : [ new_mention_id1, sentence1 ],
                        mention_id2 : [ new_mention_id2, sentence2 ],
                        ...
                        }
            new_mention_id: indexes of the adjective and noun in the sentence includes spaces
            sentence: string type of raw sentence
            mapping between mentions and sentences
        """
        mid_sent = dict()
        for filename in os.listdir(self.path_mid_orisent):
            filepath = self.path_mid_orisent + filename
            with open(filepath) as json_data:
                mid_orisentence = json.load(json_data)
                for old_ment, new_sent in mid_orisentence.items():
                    mid_sent[old_ment] = new_sent
        return mid_sent

    def read_positive(self):
        # /home/jakob/samesh/repo/bandit_crowdsourcing/data\label/pairs_label/pair_label.json
        """
            format: {'product name':
                        ['pos_pair', 'pos_pair', ...],
                     ...
                    }
        """
        positive_pair = dict()
        with open(self.path_label, 'r') as pair_file:
            pair_file.seek(0)
            pair_label = json.load(pair_file)
            for product, pairs in pair_label.items():
                positive_pair[product] = [pair for pair, label in pairs.items() if label == 'P']
        return positive_pair

    def read_cs_label(self):
        # /home/jakob/samesh/repo/bandit_crowdsourcing/data\label/mention_label/mention_label.json
        """
            format:
                { 'mention_id' : label }
            all mention labels after manually annotate
        """
        with open(self.path_cs_label, 'r') as cs_label_file:
            cs_label = json.load(cs_label_file)
        return cs_label

    def read_pickle_sparse(self):
        # /home/jakob/samesh/repo/bandit_crowdsourcing/data\feature/new_pickle_sparse/*.pickle
        """
            format: { key: mention_id; value: feature_vector }
            give the feature vector for each mention
        """
        pickle_fea = {}
        for filename in os.listdir(self.path_pickle):
            filepath = self.path_pickle + filename
            with open(filepath, 'rb') as file:
                data = pickle.load(file, encoding="latin1")
                for mention_id, feature_vector in data.items():
                    pickle_fea[mention_id] = feature_vector
        # print(pickle_fea)

        return pickle_fea


if __name__ == '__main__':
    initParm = dict(
        pair_mid='/home/jakob/samesh/repo/bandit_crowdsourcing/data/raw/pair_mention/',
        id_file='/home/jakob/samesh/repo/bandit_crowdsourcing/data/raw/id/',
        path_orisentence="/home/jakob/samesh/repo/bandit_crowdsourcing/data/raw/mapping/mid_origin/",
        pair_label='/home/jakob/samesh/repo/bandit_crowdsourcing/data/label/pairs_label/pair_label.json',
        cs_label='/home/jakob/samesh/repo/bandit_crowdsourcing/data/label/mention_label/mention_label.json',
        ground_truth='/home/jakob/samesh/repo/bandit_crowdsourcing/data/label/mention_label/ground_truth.json',
        path_pickle="/home/jakob/samesh/repo/bandit_crowdsourcing/data/feature/new_pickle_sparse/",
        feature_json_path='/home/jakob/samesh/repo/bandit_crowdsourcing/data/feature/json_index/'
    )
    ex = data_extract(initParm)
