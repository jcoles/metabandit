# for simulating a real annotator that provide ground truth labels
import json
from abc import ABC, abstractmethod

class get_groundtruth(ABC):
    def __init__(self):
        self.path_ground_truth = 'data/label/mention_label/ground_truth.json'
        
        with open(self.path_ground_truth)as json_file:
            self.ground_truth = json.load(json_file)

    def get_groundtruth(self, id):
        """
            id: mention_id of one specific mention input
            return 0/1 the groundtruth of that mention
        """
        print('ground truth for', id, 'is', self.ground_truth.get(id))
        return self.ground_truth.get(id)
