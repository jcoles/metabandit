import json
import os
import pickle
from abc import ABC, abstractmethod


# request_labels() will ask annotator for labels of mentions in a bag
class request_labels(ABC):
    def __init__(self):
        self.path_pair_mid = 'data/raw/pair_mention/'
        self.path_id = 'data/raw/id/'
        self.product_list = [product.split('_id', 1)[0] for product in os.listdir(self.path_id)]
        self.path_ground_truth = 'data/label/mention_label/ground_truth.json'

        self.pair_mention = {}
        pair_mention = {}
        for product in self.product_list:
            with open(self.path_pair_mid + product + '.json', 'r') as pair_mid_file:
                self.pair_mention[product] = json.load(pair_mid_file)

    @abstractmethod
    def request_labels(self, pair, product):
        # input: pair id (string)  e.g. good_music.auto
        # return: key - mention_id;  value - label(0/1)
        request_mention = []
        # get all mentions in this bag
        try:
            if product in self.product_list:
                request_mention = self.pair_mention[product].get(pair)
            print('mentions in bag', pair, 'are ', request_mention)
        except IndexError:
            print('Format of the input pair id is incorrect')
        except TypeError:
            print('The pair of words have no corresponding mentions')
        return request_mention


# evaluation_metric() return dictionary of bag_id(adj_n.product); mention_id; label in main memory
class evaluation_metric(ABC):
    def __init__(self):
        self.path_pair_mid = 'data/raw/pair_mention/'
        self.path_id = 'data/raw/id/'
        self.product_list = [product.split('_id', 1)[0] for product in os.listdir(self.path_id)]
        self.path_ground_truth = 'data/label/mention_label/ground_truth.json'

        self.pair_mention = {}
        for product in self.product_list:
            with open(self.path_pair_mid + product + '.json', 'r') as pair_mid_file:
                self.pair_mention[product] = json.load(pair_mid_file)

        with open(self.path_ground_truth, 'r') as ground_truth:
            self.ground_truth = json.load(ground_truth)

    def evaluate(self):
        # key: bag_id (adj_n.product)
        # value: [{mention_id1: label1}, {mention_id2: label2}, ... {m3: l3}]
        result = {}
        for product in self.product_list:
            for bag, mentionlist in self.pair_mention[product].items():
                value_save = []
                for i in range(0, len(mentionlist)):
                    one_mention = {}
                    if self.ground_truth.get(mentionlist[i]) != None:
                        one_mention[mentionlist[i]] = self.ground_truth.get(mentionlist[i])
                    else:
                        one_mention[mentionlist[i]] = 0  # negative pairs have label 0
                    # store ground truth for one mention
                    value_save.append(one_mention)
                result[bag] = value_save
        print(result)
        return result
