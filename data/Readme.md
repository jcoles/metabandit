- The full dataset is in repository:
https://bitbucket.org/Jiaxin_Liu/re_dataset

- data_extract.py include methods of converting files from all the following directorys to dict():    
data/raw/pair_mention/xx.json           
data/raw/id/xx_id.txt            
data/raw/mapping/mid_origin/xx.json          
data/label/pairs_label/pair_label.json          
data/label/mention_label/mention_label.json        
data/label/mention_label/ground_truth.json             
data/feature/new_pickle_sparse/xx.pickle         
                   Note: get_all_truth() and get_ppm_truth() are two different baselines

- plot.py: the uniform format to plot the graph    

- data_handle_graphical.py: before running this, create an empty folder called "clean_data" in data folder. It is for graphical model uses only.      

- edge_feature.py: used to create edges.pkl, edge_feature.json. This should run after data_handle_graphical.py already finished.