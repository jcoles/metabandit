import numpy as np
import json
import pickle
import csv


def load_json(path):
    f = open(path)
    dictionary = json.load(f)
    credit_card = []
    label = []
    for key, values in dictionary.items():
        credit_card.append(key)
        label.append(values)
    return [credit_card, label]

def load_pk_file(file_path):
    with open(file_path, 'rb') as f:
        data = pickle.load(f, encoding="latin1")
    return data


if __name__ == '__main__':
    all_id = []
    dataset_id = []
    feature = []
    for i, data_path in enumerate(["Apex AD2600 Progressive-scan DVD player", "Canon G3",
                                   "Creative Labs Nomad Jukebox Zen Xtra 40GB", "Nikon coolpix 4300", "Nokia 6610"]):
        # temp_data_dict = load_pk_file("/data/data/feature/pickle_sparse/{}.pickle".format(data_path))
        temp_data_dict = load_pk_file("data/feature/new_pickle_sparse/{}.pickle".format(data_path))
        for key in temp_data_dict:
            all_id.append(key)
            dataset_id.append(i)
            # print (temp_data_dict[key].toarray())
            feature.append(temp_data_dict[key].toarray()[0])
        # break
    # break
    # print(len(feature))
    # all_feature = np.array(feature)
    # print(all_feature.shape)

    # temp_positive_id, temp_positive_ground_truth = load_json("/data/data/label/mention_label/mention_label.json")
    temp_positive_id, temp_positive_ground_truth = load_json(
        "data/label/mention_label/ground_truth.json")  # json's positive's order,luan de
    # positive_label_dict = {}
    # for i,temp in enumerate(positive_id):
    # 	positive_label_dict[temp] = positive_ground_truth[i]
    # print(len(positive_id))  #2117
    # print(len(positive_ground_truth))

    ###deal positive id ###################################
    positive_id = []
    positive_ground_truth = []
    for i, temp_id in enumerate(all_id):
        if temp_id in temp_positive_id:
            positive_id.append(temp_id)
            positive_ground_truth.append(temp_positive_ground_truth[temp_positive_id.index(temp_id)])

    ########positive id/belong and negative id/belong#######
    negative_id = []
    negative_belong = []
    positive_belong = []
    for i, temp_id in enumerate(all_id):
        if temp_id not in positive_id:
            negative_id.append(temp_id)
            negative_belong.append(dataset_id[i])
        else:
            positive_belong.append(dataset_id[i])
    # print (len(negative_id))
    # print(len(negative_belong))
    # print(len(positive_belong))

    #######positive matrix and negtive matrix###############

    positive_data = []
    negative_data = []
    for i, temp_id in enumerate(all_id):
        if temp_id in positive_id:
            positive_data.append(feature[i])
        else:
            negative_data.append(feature[i])
    positive_data = np.array(positive_data)
    negative_data = np.array(negative_data)
    # print(positive_data.shape) #2107*32432
    # print(negative_data.shape) #5295*32432

    ########save_data########################################
    ####positive#############################################
    positive_belong = np.array(positive_belong)
    np.save("clean_data/positive_belong_to_which_dataset.npy", positive_belong)

    f = open('clean_data/positive_id.txt', 'w')
    for temp in positive_id:
        f.write(temp)
        f.write("\n")
    f.close()

    np.save("clean_data/positive_data.npy", positive_data)

    positive_ground_truth = np.array(positive_ground_truth)
    np.save("clean_data/positive_ground_truth.npy", positive_ground_truth)

    ####negative#############################################
    negative_belong = np.array(negative_belong)
    np.save("clean_data/negative_belong_to_which_dataset.npy", negative_belong)

    f = open('clean_data/negative_id.txt', 'w')
    for temp in negative_id:
        f.write(temp)
        f.write("\n")
    f.close()

    np.save("clean_data/negative_data.npy", negative_data)
