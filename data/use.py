# main class 
# sample ways to use those abstract classes
import json
import os
from bandit import request_labels
from annotator import get_groundtruth
from bandit import evaluation_metric
from plot import plot


# the way to use request_labels abstract class
class abstractclassEx(request_labels):
    # call the request_labels method in the abstract class
    def request_labels(self, pair, product):
        super().request_labels(pair, product)   

# the way to use get_groundtruth abstract class
class abstractclassGr(get_groundtruth):
    # call the request_labels method in the abstract class
    def get_groundtruth(self, id):
        super().get_groundtruth(id)   

class abstractclassEval(evaluation_metric):
    def evaluate(self):
        super().evaluate()

if __name__ == '__main__':
    # the ways to call methods in bandit.py and annotator.py
    x = abstractclassEx()
    x.request_labels("perfect_quality", "Nokia 6610")

    y = abstractclassEval()
    y.evaluate()
    
    # example:
    z = abstractclassGr()
    z.get_groundtruth('8b15359b7863064bb6750c1a814f9531.(3, 5).Nokia')
