import json
import os
import re
import progressbar
import pickle



#########Define edge features
def parse_mid(mid, pair):
    """
        parse a mention id into sentence id, indices of the two words, and the pair of (adj, noun)
    """
    parts = mid.split('.')
    sent_id = parts[0]
    indices = [int(i) for i in parts[1][1:len(parts[1])-1].split(', ')]
    pairs = pair.split('_')
    
    return sent_id, indices, pairs

def mention_features(mid, pair,
                     n_n,
                     n_adj,
                     positive_noun_freq,
                     positive_adj_freq,
                     negative_noun_freq,
                     negative_adj_freq):
    """
        construct features for a mention on an edge.
    """
    
    _, indices, pairs = parse_mid(mid, pair)
    f1 = indices[1] - indices[0]
    f2 = 1 if pairs[1] in n_n else 0
    f3 = 1 if pairs[0] in n_adj else 0

    if pairs[1] in negative_noun_freq:
        f4 = negative_noun_freq[pairs[1]]
    else:
        f4 = 0
    
    if pairs[1] in positive_noun_freq:
        f5 = positive_noun_freq[pairs[1]]
    else:
        f5 = 0
    
    if pairs[0] in negative_adj_freq:
        f6 = negative_adj_freq[pairs[0]]
    else:
        f6 = 0
        
    if pairs[0] in positive_adj_freq:
        f7 = positive_adj_freq[pairs[0]]
    else:
        f7 = 0
    
    return [f1, f2, f3, f4, f5, f6, f7]

def same_sentence_same_noun_loc(mid_1, mid_2, pair_1, pair_2,
                                id_sent_dic,
                                conj_and,
                                conj_but):
    """
        construct features for a pair of mentions that are in the same sentence,
        sharing the same noun (at the same location)
        but have different adjs.
        
    Args:
        mid_1: mention id 1
        mid_2: mention id 2
        id_sent_dic: mapping from sentence id to sentence tokens
        conj_and: conjunctions and's
        conj_but: conjunctions but's
    Return:
        a dictionary of edge features if the condition is satisfied
        None otherwise
    """
    sent_id_1, indices_1, pairs_1 = parse_mid(mid_1, pair_1)
    sent_id_2, indices_2, pairs_2 = parse_mid(mid_2, pair_2)
    
    if sent_id_1 == sent_id_2 and indices_1[1]==indices_2[1] and pairs_1[0]!=pairs_2[0]:
            
        featureEdge = {}
        
        # distance between adj and noun
        pd1 = indices_1[1] - indices_1[0]
        pd2 = indices_2[1] - indices_2[0]

        tokens = id_sent_dic[sent_id_1]
        
        start, end = min(indices_1[0], indices_2[0]), max(indices_1[0], indices_2[0])
        featureEdge['CCBA'] = 1 if start + 2 == end and tokens[start+1] in conj_and else 0
        featureEdge['ACBA'] = 1 if start + 2 == end and tokens[start+1] in conj_but else 0
        featureEdge['SBA'] = 1 if ',' in tokens[start:end] else 0
        featureEdge['NWBA'] = 1 if start + 1 == end else 0
        featureEdge['NTS'] = 1 if pd1 * pd2 < 0 else 0

        # featureEdge['SS'] = 1

        featureEdge['category'] = 1
        
        return featureEdge
    else:
        return None
    
def same_sentence_same_adj_loc(mid_1, mid_2, pair_1, pair_2,
                               id_sent_dic,
                               conj_and,
                               conj_but):
    """
        construct features for a pair of mentions that are in the same sentence,
        sharing the same adjective (at the same location)
        but have different nouns.
        
    Args:
        mid_1: mention id 1
        mid_2: mention id 2
        id_sent_dic: mapping from sentence id to sentence tokens
        conj_and: conjunctions and's
        conj_but: conjunctions but's
    Return:
        a dictionary of edge features if the condition is satisfied
        None otherwise
    """
    sent_id_1, indices_1, pairs_1 = parse_mid(mid_1, pair_1)
    sent_id_2, indices_2, pairs_2 = parse_mid(mid_2, pair_2)
    
    if sent_id_1 == sent_id_2 and indices_1[0] == indices_2[0] and pairs_1[1] != pairs_2[1]:

        featureEdge = {}
        
        # distance between adj and noun
        pd1 = indices_1[1] - indices_1[0]
        pd2 = indices_2[1] - indices_2[0]

        tokens = id_sent_dic[sent_id_1]

        start, end = min(indices_1[1], indices_2[1]), max(indices_1[1], indices_2[1])
        featureEdge['CCBN'] = 1 if start + 2 == end and tokens[start+1] in conj_and else 0
        featureEdge['ACBN'] = 1 if start + 2 == end and tokens[start+1] in conj_but else 0
        featureEdge['SBN'] = 1 if ',' in tokens[start:end] else 0
        featureEdge['NWBN'] = 1 if start + 1 == end else 0
        featureEdge['ATS'] = 1 if pd1 * pd2 < 0 else 0

        # featureEdge['SS'] = 1

        featureEdge['category'] = 2
        
        return featureEdge
    
    else:
        return None

def same_sentence_same_pair(mid_1, mid_2, pair_1, pair_2,
                           id_pair_dic):
    """
    Args:
        mid_1: mention id 1
        mid_2: mention id 2
        id_sent_dic: mapping from sentence id to sentence tokens
    Return:
        a dictionary of edge features if the condition is satisfied
        None otherwise
    """
    sent_id_1, indices_1, pairs_1 = parse_mid(mid_1, pair_1)
    sent_id_2, indices_2, pairs_2 = parse_mid(mid_2, pair_2)
    
    if sent_id_1 == sent_id_2 and pair_1 == pair_2 and (indices_1[1] != indices_2[1] or indices_1[0] != indices_2[0]):
        
        featureEdge = {}
        
        featureEdge['NMB'] = len(id_pair_dic[pair_1])
        # featureEdge['SS'] = 1

        featureEdge['category'] = 6
        
        return featureEdge
    else:
        return None
    
def two_sentences_same_noun(mid_1, mid_2, pair_1, pair_2):
    """
    Args:
        mid_1: mention id 1
        mid_2: mention id 2
    Return:
        a dictionary of edge features if the condition is satisfied
        None otherwise
    """
    sent_id_1, indices_1, pairs_1 = parse_mid(mid_1, pair_1)
    sent_id_2, indices_2, pairs_2 = parse_mid(mid_2, pair_2)
    if sent_id_1 != sent_id_2 and pairs_1[1] == pairs_2[1] and pairs_1[0] != pairs_2[0]:
        # featureEdge['SS'] = 0
        featureEdge = {'category':3}
        return featureEdge
    else:
        return None

def two_sentences_same_adj(mid_1, mid_2, pair_1, pair_2):
    """
    Args:
        mid_1: mention id 1
        mid_2: mention id 2
    Return:
        a dictionary of edge features if the condition is satisfied
        None otherwise
    """
    sent_id_1, indices_1, pairs_1 = parse_mid(mid_1, pair_1)
    sent_id_2, indices_2, pairs_2 = parse_mid(mid_2, pair_2)
    if sent_id_1 != sent_id_2 and pairs_1[0] == pairs_2[0] and pairs_1[1] != pairs_2[1]:
        # featureEdge['SS'] = 0
        featureEdge = {'category':4}
        return featureEdge
    else:
        return None

def two_sentences_same_pair(mid_1, mid_2, pair_1, pair_2):
    sent_id_1, indices_1, pairs_1 = parse_mid(mid_1, pair_1)
    sent_id_2, indices_2, pairs_2 = parse_mid(mid_2, pair_2)
    if sent_id_1 != sent_id_2 and pairs_1[1] == pairs_2[1] and pairs_1[0] == pairs_2[0]:
        # featureEdge['SS'] = 0
        # number of mentions for this pair (bag size)
#         print ('_'.join(pairs_1), '_'.join(pairs_2))
        featureEdge = {'category': 5}
        featureEdge['NMB'] = len(id_pair_dic['_'.join(pairs_1)])
        return featureEdge
    else:
        return None



if __name__ == '__main__':

    n_n = set(['time', 'overview', 'mirror', 'day', 'some', 'moment', 'option', 'thing', 'things',
           'weeks', 'times', 'reviews', 'back-ups', 'shows', 'process', 'way', 'deals', 'people',
           'place', 'years', 'difference', 'reviewers', 'monster', 'requirements', 'file',
           'users', 'night', 'hearing', 'pair', 'staff', 'chance', 'challenge', 'scenes', 'arrangement'])
    n_adj = set(['several', 'new', 'different', 'more', 'other', 'third', 'few', 'only', 'first', 'certain', 'little',
             'above','left', 'photo', 'photos', 'many', 'full', 'shutter', 'entire', 'sound', 'doesnt', 'mp3', 'extraneous',
             'peripheral','creative', 'most', 'extra', 'audio', 'main', '40gb', 'xtra', 'next', 'indoor', 'previous', 'american',
             'your'])
    conj_and = set(['and', 'or', 'also', 'both'])
    conj_but = set(['but', 'yet', 'otherwise', 'still', 'nevertheless', 'while', 'whereas', 'however'])


    id_sent_path = 'data/raw/id/' # sentence id
    id_pair_mention = 'data/raw/pair_mention/' # pair's all mention
    pair_label_path = 'data/label/pairs_label/pair_label.json' # pair's label
    edges_path = 'clean_data/edges.pkl'
    edges_json = 'clean_data/edge_feature/'
    ground_truth_path = 'data/label/mention_label/ground_truth.json'

    ground_truthdic = {}    # all positive bags with their label
    with open(ground_truth_path)as json_file:
        data = json.load(json_file)
        for key,value in data.items():
            ground_truthdic[key] = value

    id_sent_dic = {}
    id_pair_dic = {}
    mentionid_pair = {}
    pair_PN_dic = {}
        
    for product in sorted(os.listdir(id_sent_path)): # sentence id-sentence dict
        with open(id_sent_path+product, 'r') as s:
            for lines in s:
                parts = lines.split(' ', 1)
                id_sent_dic[str(parts[0])] = str(parts[1].split('\n')[0])

    # tokenize the sentence
    cop = re.compile("[^ ^a-z^A-Z^0-9]")
    for k, v in id_sent_dic.items():
        id_sent_dic[k] = cop.sub('', v).split()
        
    print (len(id_sent_dic))
    # with open('../../clean_data/sentenceid_sentence.json', 'w') as f:
        # json.dump(id_sent_dic, f, ensure_ascii=False, indent=4)
    
    # For create edges including negative ones
    # store all mentions and their bag
    for product in sorted(os.listdir(id_pair_mention)):
        with open(id_pair_mention+product) as p:
            dic = json.load(p)
            id_pair_dic.update(dic) # all pair-mention id
            for key in dic.keys():
                for value in dic[key]:
                    mentionid_pair[value] = product.split('.')[0]+'.'+key # all mention id-pair
    with open('clean_data/mentionid_pair.json', 'w') as f:
        json.dump(mentionid_pair, f, ensure_ascii=False, indent=4)
    
    # store positive mentions and their bag
    mentionid_pair = {}  # foramt: key--bagid.prodname, value--mentionid
    pos_num = 0
    for product in sorted(os.listdir(id_pair_mention)):
        with open(id_pair_mention+product) as p:
            data = json.load(p)
            prod = product.split('.')[0]
            
            id_pair_dic.update(data)
            for key, value in data.items():
                for i in range(len(value)):
                    try:
                        if ground_truthdic[value[i]] != None:   # is a mention in positive bag
                            if ground_truthdic[value[i]] == 1:
                                pos_num += 1
                            key_stored = prod + '.' + key
                            mentionid_pair[value[i]] = key_stored
                    except KeyError:
                        a=1
    with open('clean_data/mentionid_pospair.json', 'w') as f:
        json.dump(mentionid_pair, f, ensure_ascii=False, indent=4)

    for k,v in id_pair_dic.items():
        print (k, v)
        break
    print('pos mention', pos_num)
    print('len mention id pair', len(mentionid_pair))#7402=2117+5285

    with open(pair_label_path) as label:
        temp = json.load(label)
        for value in temp.values(): # all pair-label
            for pair,label in value.items():
                if label=='N':
                    pair_PN_dic['N']=pair_PN_dic.get('N', [])+[pair]
                else:
                    pair_PN_dic['P']=pair_PN_dic.get('P', [])+[pair]

    positive_noun = [x.split('_')[1] for x in pair_PN_dic['P']]
    positive_adj = [x.split('_')[0] for x in pair_PN_dic['P']]
    positive_noun_freq = {x:positive_noun.count(x) for x in positive_noun}
    positive_adj_freq = {x:positive_adj.count(x) for x in positive_adj}
    print (len(positive_noun_freq))
    print (len(positive_adj_freq))

    negative_noun = [x.split('_')[1] for x in pair_PN_dic['N']]
    negative_adj = [x.split('_')[0] for x in pair_PN_dic['N']]
    negative_noun_freq = {x:negative_noun.count(x) for x in negative_noun}
    negative_adj_freq = {x:negative_adj.count(x) for x in negative_adj}
    print (len(negative_noun_freq))
    print (len(negative_adj_freq))


    #########Construct edge features for all pairs of mentions
    # storing all edges constructed so far
    edges = {}

    i = 0
    bar = progressbar.ProgressBar(maxval=len(mentionid_pair), \
        widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
    bar.start()

    for mid_1, pairprod_1 in mentionid_pair.items():
        pair_1 = pairprod_1.split('.')[1]
    #     print (mid_1)
        i += 1
        if i % 100 == 0:
            bar.update(i+1)
            
        for mid_2, pair_prod2 in mentionid_pair.items():
            pair_2 = pair_prod2.split('.')[1]
    #         print (mid_2)
            if mid_1 == mid_2:
                continue
            if (mid_1 + '-' + mid_2) in edges or (mid_2 + '-' + mid_1) in edges:
                continue
            
            m_feature_1 = mention_features(mid_1, pair_1,
                                           n_n,
                                           n_adj,
                                           positive_noun_freq,
                                           positive_adj_freq,
                                           negative_noun_freq,
                                           negative_adj_freq)
            m_feature_1 = {'PD1':m_feature_1[0],
                          'IN1':m_feature_1[1],
                          'IA1':m_feature_1[2],
                          'NN1NP':m_feature_1[3],
                          'NN1PP':m_feature_1[4],
                          'NA1NP':m_feature_1[5],
                          'NA1PP':m_feature_1[6]}
            
            m_feature_2 = mention_features(mid_2, pair_2,
                                           n_n,
                                           n_adj,
                                           positive_noun_freq,
                                           positive_adj_freq,
                                           negative_noun_freq,
                                           negative_adj_freq)
            m_feature_2 = {'PD2':m_feature_2[0],
                          'IN2':m_feature_2[1],
                          'IA2':m_feature_2[2],
                          'NN2NP':m_feature_2[3],
                          'NN2PP':m_feature_2[4],
                          'NA2NP':m_feature_2[5],
                          'NA2PP':m_feature_2[6]}
    #         mention_1_featuers = mention_features(mid_1, pair_n_str, pair_p_str)
    #         mention_2_featuers = mention_features(mid_2, pair_n_str, pair_p_str)
            
            featureEdge = same_sentence_same_noun_loc(mid_1, mid_2, pair_1, pair_2,
                                                      id_sent_dic,
                                                      conj_and,
                                                      conj_but)
            if featureEdge is not None:
                featureEdge.update(m_feature_1)
                featureEdge.update(m_feature_2)
                edges[mid_1 + '-' + mid_2] = featureEdge
                continue
            
            featureEdge = same_sentence_same_adj_loc(mid_1, mid_2, pair_1, pair_2,
                                                      id_sent_dic,
                                                      conj_and,
                                                      conj_but)
            if featureEdge is not None:
                featureEdge.update(m_feature_1)
                featureEdge.update(m_feature_2)
                edges[mid_1 + '-' + mid_2] = featureEdge
                continue
            
            featureEdge = same_sentence_same_pair(mid_1, mid_2, pair_1, pair_2,
                                                  id_pair_dic)
            if featureEdge is not None:
                featureEdge.update(m_feature_1)
                featureEdge.update(m_feature_2)
                edges[mid_1 + '-' + mid_2] = featureEdge
                continue
                
            featureEdge = two_sentences_same_noun(mid_1, mid_2, pair_1, pair_2)
            
            if featureEdge is not None:
                featureEdge.update(m_feature_1)
                featureEdge.update(m_feature_2)
                edges[mid_1 + '-' + mid_2] = featureEdge
                continue
                
            featureEdge = two_sentences_same_adj(mid_1, mid_2, pair_1, pair_2)
            if featureEdge is not None:
                featureEdge.update(m_feature_1)
                featureEdge.update(m_feature_2)
                edges[mid_1 + '-' + mid_2] = featureEdge
                continue
                
            featureEdge = two_sentences_same_pair(mid_1, mid_2, pair_1, pair_2)
            if featureEdge is not None:
                featureEdge.update(m_feature_1)
                featureEdge.update(m_feature_2)
                edges[mid_1 + '-' + mid_2] = featureEdge
                continue
            # break
        # break
    bar.finish()
    print ('number of edges = {}'.format(len(edges)))

    
    with open(edges_json+'edge_feature.json', 'w') as f:
        json.dump(edges, f, ensure_ascii=False, indent=4)


    with open(edges_path, 'wb') as f:
        pickle.dump(edges, f)
        